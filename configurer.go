package c4qblog

import (
	"io"

	"git.fractalqb.de/fractalqb/c4hgol"
	"git.fractalqb.de/fractalqb/qblog"
)

var levelMap = c4hgol.LevelMap{
	Std: []c4hgol.Level{
		c4hgol.LeastImportant,
		c4hgol.Trace,
		c4hgol.Debug,
		c4hgol.Info,
		c4hgol.Warn,
		c4hgol.Error,
		c4hgol.MostImportant,
	},
	Impl: []int32{
		int32(qblog.LmostIrrelevant),
		int32(qblog.Ltrace),
		int32(qblog.Ldebug),
		int32(qblog.Linfo),
		int32(qblog.Lwarn),
		int32(qblog.Lerror),
		int32(qblog.LmostImportant),
	},
}

func NewConfig(log *qblog.Logger) Configurer {
	res := Configurer{
		log: log,
		lvl: log.Level,
	}
	return res
}

type Configurer struct {
	log *qblog.Logger
	lvl qblog.Level // remember level for disable/enable
}

func (cfg Configurer) Name() string { return cfg.log.Title }

func (cfg Configurer) Enabled() bool { return cfg.log.Level != qblog.Loff }

func (cfg Configurer) Enable(setEnabled bool) {
	if setEnabled {
		cfg.log.Level = cfg.lvl
	} else {
		cfg.lvl = cfg.log.Level
		cfg.log.Level = qblog.Loff
	}
}

func (cfg Configurer) Level() c4hgol.Level {
	res := levelMap.ToStd(int32(cfg.log.Level))
	return res
}

func (cfg Configurer) SetLevel(l c4hgol.Level) {
	set := levelMap.FromStd(l)
	cfg.log.Level = uint8(set)
}

func (cfg Configurer) Output() io.Writer { return cfg.log.Output() }

func (cfg Configurer) SetOutput(wr io.Writer) {
	cfg.log.SetOutput(wr)
}

func (cfg Configurer) ShowSource(mode c4hgol.ShowSrcMode) {
	flags := cfg.log.Flags() & ^uint32(qblog.FsrcMask)
	switch mode {
	case c4hgol.ShowSrcFile:
		flags |= qblog.FsrcFile
	case c4hgol.ShowSrcPath:
		flags |= qblog.FsrcPath
	case c4hgol.ShowStack:
		flags |= qblog.FsrcStack
	}
	cfg.log.SetFlags(flags)
}
