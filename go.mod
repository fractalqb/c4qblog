module git.fractalqb.de/fractalqb/c4qblog

go 1.13

require (
	git.fractalqb.de/fractalqb/c4hgol v0.12.0
	git.fractalqb.de/fractalqb/qblog v0.11.0
)
