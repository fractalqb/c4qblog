# c4qblog

[![GoDoc](https://godoc.org/github.com/fractalqb/c4qblog?status.svg)](https://pkg.go.dev/git.fractalqb.de/fractalqb/c4qblog)

This lib is deprecated. [qblog](https://codeberg.org/fractalqb/qblog)
now natively supports configuration via
[c4hgol](https://codeberg.org/fractalqb/c4hgol).
